#include <engine.hpp>

Engine::Engine(){
    this->resolution = Vector2f(450, 450);
    this->window.create(VideoMode(resolution.x,resolution.y), "2048", Style::Titlebar | Style::Close);
    //initialise plateau 
    for (size_t i = 0; i < 4; i++)    {
        this->tabCase.push_back(vector<Case>(4,Case()));
    }   
    
    
}


void Engine::run(){
    if(window.isOpen()){    
        drawTabCase();        
    }

    //Loop run until the window is closed 

    while (window.isOpen()){

        //Event
        while(window.pollEvent(ev)){
            switch (ev.type){ 
                case Event::Closed:
                    window.close();
                    break;
                
                case Event::KeyPressed:
                    if(ev.key.code == Keyboard::Escape){
                        window.close();
                        break;
                    }

                    // UP
                    if(ev.key.code == Keyboard::Z || ev.key.code == Keyboard::Up){
                        goUp();
                        drawTabCase();
                    }

                    // DOWN
                    if(ev.key.code == Keyboard::S || ev.key.code == Keyboard::Down){
                        goDown();
                        drawTabCase();
                    }

                    // LEFT
                    if(ev.key.code == Keyboard::Q || ev.key.code == Keyboard::Left){
                        goLeft();
                        drawTabCase();  
                    }

                    //RIGHT
                    if(ev.key.code == Keyboard::D || ev.key.code == Keyboard::Right){
                        goRight();
                        drawTabCase();
                    }
                    break;
            }
        }     

        
    }
    
}