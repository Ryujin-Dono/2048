#include <engine.hpp>

/*
ATTENTION: when we double loop tabCase it's in that order: row and column 
*/

void Engine::drawTabCase(){
    //Clear
    window.clear(Color(8, 32, 50));

    // we creat a tab to stock all the position where it is -1
    vector<Vector2i> posPossible;
    for (size_t x=0; x< tabCase.size(); x++){
        for (size_t y = 0; y < tabCase[x].size(); y++){
            if(tabCase[x][y].getNumber() == -1){
                posPossible.push_back(Vector2i(x,y));
            }
        }
    }

    // perdu si il n'y a plus d'espace
    if(posPossible.empty()){
        Text text;
        Font font;
        text.setCharacterSize(50);
        text.setFillColor(Color(255, 76, 41));
        if(!font.loadFromFile("../assets/fonts/slant_regular.ttf")){
            cerr << "error" << endl;
        }
        text.setFont(font);
        text.setString(string("TA PERDU"));
        text.setPosition(Vector2f(text.getLocalBounds().width - 100, 225 - text.getLocalBounds().height));

        window.draw(text);
    }

    else{
        // after that we tae a random number and set the postion corresponding to 1q
        int rand1 = rand() % posPossible.size();
        tabCase[posPossible[rand1].x][posPossible[rand1].y].setNumber(1);
    
    
        //draw plateau  
        for (size_t x=0; x< tabCase.size(); x++){
            for (size_t y = 0; y < tabCase[x].size(); y++){
                window.draw(tabCase[x][y].drawCase(Vector2f(x*100 + x*10 +10, y*100 + y*10 +10)));
                tabCase[x][y].setText(tabCase[x][y].getNumber());
                window.draw(tabCase[x][y].drawText());            
            }
            
        }
    }

    //finish drawing
    window.display(); // tell the app the drawing done's

}

void Engine::goLeft(){    
    int temp=0;

    for (size_t i = 0; i < tabCase.size(); i++){
        for (size_t y = 0; y < tabCase[i].size(); y++){
            // we take a one 
            if(tabCase[i][y].getNumber() != -1 && i != 0 ){
                bool found = false;
                // we look if there is another number on is left
                for (int n = i - 1; n >= 0; n--)
                {
                    // if there is one then we look if there are the same
                    if(tabCase[n][y].getNumber() != -1){
                        // they are the same so we add them together
                        if(tabCase[n][y].areSame(tabCase[i][y])){                            

                            tabCase[n][y].setNumber(tabCase[i][y].getNumber() + tabCase[n][y].getNumber());
                            tabCase[i][y].setNumber(-1);
                            found = true;
                            break;
                        }
                        // they are not so we set the number we took to be on the right of him
                        else{
                            temp = tabCase[i][y].getNumber();
                            tabCase[i][y].setNumber(-1);

                            tabCase[n+1][y].setNumber(temp);
                            found = true;
                            break;
                        }      
                    }                    
                }
                // if no number was found then we set is position to the first case on the left
                if(!found){
                    tabCase[0][y].setNumber(tabCase[i][y].getNumber());
                    tabCase[i][y].setNumber(-1);
                }
            }
            
        }            
    }    
}

void Engine::goRight(){    
    int temp=0;

    for (size_t i = 0; i < tabCase.size(); i++){
        for (size_t y = 0; y < tabCase[i].size(); y++){
            // we take a one 
            if(tabCase[i][y].getNumber() != -1 && i != tabCase.size()-1 ){
                bool found = false;
                // we look if there is another number on is right
                for (int n = i + 1 ; n < tabCase.size(); n++)
                {
                    // if there is one then we look if there are the same
                    if(tabCase[n][y].getNumber() != -1){
                        // they are the same so we add them together
                        if(tabCase[n][y].areSame(tabCase[i][y])){                            
                            tabCase[n][y].setNumber(tabCase[i][y].getNumber() + tabCase[n][y].getNumber());
                            tabCase[i][y].setNumber(-1);
                            found = true;
                            break;
                        }
                        // they are not so we set the number we took to be on the left of him
                        else{
                            temp = tabCase[i][y].getNumber();
                            tabCase[i][y].setNumber(-1);

                            tabCase[n-1][y].setNumber(temp);
                            found = true;
                            break;
                        }      
                    }                    
                }
                // if no number was found then we set is position to the first case on the right
                if(!found){
                    tabCase[tabCase.size()-1][y].setNumber(tabCase[i][y].getNumber());
                    tabCase[i][y].setNumber(-1);
                }
            }
            
        }            
    }    
}

void Engine::goUp(){    
    int temp=0;

    for (size_t i = 0; i < tabCase.size(); i++){
        for (size_t y = 0; y < tabCase[i].size(); y++){
            // we take a one 
            if(tabCase[i][y].getNumber() != -1 && y != 0 ){
                bool found = false;
                // we look if there is another number on is left
                for (int n = y - 1; n >= 0; n--)
                {
                    // if there is one then we look if there are the same
                    if(tabCase[i][n].getNumber() != -1){
                        // they are the same so we add them together
                        if(tabCase[i][n].areSame(tabCase[i][y])){                            

                            tabCase[i][n].setNumber(tabCase[i][y].getNumber() + tabCase[i][n].getNumber());
                            tabCase[i][y].setNumber(-1);
                            found = true;
                            break;
                        }
                        // they are not so we set the number we took to be on the right of him
                        else{
                            temp = tabCase[i][y].getNumber();
                            tabCase[i][y].setNumber(-1);

                            tabCase[i][n+1].setNumber(temp);
                            found = true;
                            break;
                        }      
                    }                    
                }
                // if no number was found then we set is position to the first case on the left
                if(!found){
                    tabCase[i][0].setNumber(tabCase[i][y].getNumber());
                    tabCase[i][y].setNumber(-1);
                }
            }
            
        }            
    }    
}

void Engine::goDown(){    
   int temp=0;

    for (size_t i = 0; i < tabCase.size(); i++){
        for (size_t y = 0; y < tabCase[i].size(); y++){
            // we take a one 
            if(tabCase[i][y].getNumber() != -1 && y != tabCase.size() ){
                bool found = false;
                // we look if there is another number on is left
                for (int n = y + 1; y < tabCase.size(); n++)
                {
                    // if there is one then we look if there are the same
                    if(tabCase[i][n].getNumber() != -1){
                        // they are the same so we add them together
                        if(tabCase[i][n].areSame(tabCase[i][y])){                            

                            tabCase[i][n].setNumber(tabCase[i][y].getNumber() + tabCase[i][n].getNumber());
                            tabCase[i][y].setNumber(-1);
                            found = true;
                            break;
                        }
                        // they are not so we set the number we took to be on the right of him
                        else{
                            temp = tabCase[i][y].getNumber();
                            tabCase[i][y].setNumber(-1);

                            tabCase[i][n-1].setNumber(temp);
                            found = true;
                            break;
                        }      
                    }                    
                }
                // if no number was found then we set is position to the first case on the left
                if(!found){
                    tabCase[i][0].setNumber(tabCase[i][y].getNumber());
                    tabCase[i][y].setNumber(-1);
                }
            }
            
        }            
    }  
}

