#include <case.hpp>

Case::Case(){
    this->number = -1;
    this->width = 100;
    this->height = 100;
    this->chrSize = 40;

    this->pos = Vector2f(0,0);

    if(!this->font.loadFromFile("../assets/fonts/slant_regular.ttf")){
        cerr << "error" << endl;
    }
}

bool Case::areSame(Case c){
    return c.getNumber() == this->getNumber();
}

RectangleShape Case::drawCase(Vector2f v){
    this->rect.setSize(Vector2f(this->getWidth(), this->getHeight()));
    this->rect.setPosition(Vector2f(v));
    this->rect.setFillColor(Color(255, 76, 41));

    this->setPos(v);

    return this->rect;
}

Text Case::drawText(){
    this->text.setCharacterSize(this->getChrSize());
    this->text.setFillColor(Color(44, 57, 75));
    this->text.setFont(this->font);
    this->text.setPosition(Vector2f((this->pos.x + this->width/2) - this->text.getLocalBounds().width, (this->pos.y + this->height/2) - this->text.getLocalBounds().height));

    return this->text;
}

int Case::getNumber(){
    return this->number;
}

int Case::getWidth(){
    return this->width;
}

int Case::getHeight(){
    return this->height;
}

int Case::getChrSize(){
    return this->chrSize;
}

void Case::setPos(Vector2f v){
    this->pos = v;
}

void Case::setNumber(int n){
    this->number = n;
}

void Case::setText(int n){
    
    if(n!=-1)
        this->text.setString(to_string(this->getNumber()));
    else{
        this->text.setString("");
    }
    
}


