#ifndef CASE_HPP
#define CASE_HPP

#include <SFML/Graphics.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/Text.hpp>
#include <fstream>
#include <stdlib.h>
#include <string>
#include<iostream>


using namespace sf;
using namespace std;

class Case{
    private:
        int number, width, height, chrSize;
        Vector2f pos;
        RectangleShape rect;
        Text text;
        Font font;

    public:
        // construtor 
        Case();

        // this method will permit to look if the two case we are looking are the same
        bool areSame(Case c);

        // method that draw the case 
        RectangleShape drawCase(Vector2f v);
        Text drawText();

        //getter 
        int getNumber();
        int getWidth();
        int getHeight();
        int getChrSize();

        //set 
        void setPos(Vector2f v);
        void setNumber(int n);
        void setText(int n);
};

#endif 
