#ifndef ENGINE_HPP
#define ENGINE_HPP


#include <SFML/Graphics.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include <vector>
#include <deque>
#include <fstream>
#include <stdlib.h>

#include <case.hpp>

using namespace sf;
using namespace std;

class Engine {
private:
    // Window
    Vector2f resolution;
    RenderWindow window;
    //Event 
    Event ev;

    //Case
    Case c;

    //Plateau
    vector<vector<Case>> tabCase; //-1:case vide / n>0:case avec chiffre 


    

public:
    enum GameState { RUNNING, GAMEOVER };
    //constructeur
    Engine();

    // The main loop will be in the run function
    void run();

    //Plateau 
    void drawTabCase();

    void goLeft();
    void goRight();
    void goDown();
    void goUp();


};


#endif 
